package org.itstep.qa.autotest.hflabsgithub;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 1.	Написать тест, проверяющий наличие всех обязательных полей и заголовков к ним
 2.	Написать тест, на отправку сообщения (решить что является показателем того, что сообщение отправлено)
 3.	Написать тест (–ы) на валидацию поля email
 4.	Написать тест на проверку автоматического заполнения полей адреса после ввода адреса в строку поиска

 */
public class TestsHflabsGithub {
    public WebDriver driver;

    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://hflabs.github.io/suggestions-demo/");
    }

   @BeforeMethod
    public void openStartPage (){
        driver.get("http://hflabs.github.io/suggestions-demo/");
    }

    @AfterClass
    public void stop() {
        driver.manage().deleteAllCookies();
        driver.close();
    }


    @Test (description="http://hflabs.github.io/suggestions-demo - Проверка наличия на странице всех обязательных полей и заголовков к ним")
    public void testExistObligatoryElements() {
        // проверим на наличие обезательные поля

        // Подпись ФИО и поле для ввода (строка поиска общая)
        Assert.assertEquals(driver.findElement(By.id("fullname")).getAttribute("placeholder"),"Введите ФИО в свободной форме");
        Assert.assertEquals(driver.findElement(By.id("fullname")).isDisplayed(),true);

        // Подпись фамилия и поле для ввода фамилии
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[2]/div[1]/div[1]/label")).getText(),"Фамилия");
        Assert.assertEquals(driver.findElement(By.id("fullname-surname")).isDisplayed(),true);

        // Подпись имя и поле для его ввода
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[2]/div[2]/div[1]/label")).getText(),"Имя");
        Assert.assertEquals(driver.findElement(By.id("fullname-name")).isDisplayed(),true);

        // Подпись отчество и поле для его ввода
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[2]/div[3]/div[1]/label")).getText(),"Отчество");
        Assert.assertEquals(driver.findElement(By.id("fullname-patronymic")).isDisplayed(),true);

        //Подпись email и поле для его ввода
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[4]/label")).getText(),"E-mail");
        Assert.assertEquals(driver.findElement(By.id("email")).isDisplayed(),true);

        //Подпись Содержание обращения и поле для его ввода
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[2]/label")).getText(),"Содержание обращения");
        Assert.assertEquals(driver.findElement(By.id("message")).isDisplayed(),true);

        // кнопка <Отправить сообщение>
        Assert.assertEquals(driver.findElement(By.cssSelector(".btn-lg")).isDisplayed(),true);
    }

    @Test (description="тест на отправку сообщения (показателем того, что сообщение отправлено - появление надписи < Это не настоящее правительство :-( > )")
    public void testSendMessage() {

        // проверим на наличие обезательные поля, введем в них данные и нажмем кнопку <отправить сообщение>
        // - критерием отправки сообщение - появление надписи <Это не настоящее правительство :-(> id="feedback-message"

        Assert.assertEquals(driver.findElement(By.id("fullname-surname")).isDisplayed(),true);
        driver.findElement(By.id("fullname-surname")).sendKeys("Иванов ");

        Assert.assertEquals(driver.findElement(By.id("fullname-name")).isDisplayed(),true);
        driver.findElement(By.id("fullname-name")).sendKeys("Петр ");

        Assert.assertEquals(driver.findElement(By.id("fullname-patronymic")).isDisplayed(),true);
        driver.findElement(By.id("fullname-patronymic")).sendKeys("Абович ");

        Assert.assertEquals(driver.findElement(By.id("email")).isDisplayed(),true);
        driver.findElement(By.id("email")).sendKeys("ivpetab@mail.ru");

        Assert.assertEquals(driver.findElement(By.id("message")).isDisplayed(),true);
        driver.findElement(By.id("message")).sendKeys("Привет!");

        Assert.assertEquals(driver.findElement(By.cssSelector(".btn-lg")).isDisplayed(),true);
        driver.findElement(By.cssSelector(".btn-lg")).click();
        //немного подождем пробежки полосы прокрутки <class = progress-bar>
        try
        {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver.findElement(By.id("feedback-message")).isDisplayed(),true);
    }

    @Test (description="тесты на валидацию поля email")
    public void testCheckValidationEmail() {

        Assert.assertEquals(driver.findElement(By.id("fullname-surname")).isDisplayed(),true);
        driver.findElement(By.id("fullname-surname")).sendKeys("Иванов ");

        Assert.assertEquals(driver.findElement(By.id("fullname-name")).isDisplayed(),true);
        driver.findElement(By.id("fullname-name")).sendKeys("Петр ");

        Assert.assertEquals(driver.findElement(By.id("fullname-patronymic")).isDisplayed(),true);
        driver.findElement(By.id("fullname-patronymic")).sendKeys("Абович ");

        Assert.assertEquals(driver.findElement(By.id("message")).isDisplayed(),true);
        driver.findElement(By.id("message")).sendKeys("Привет!");

        /* введем в неё адрес
         1 - НЕ содержащий @, содержащий домен верхнего уровня "mail.ru"
         2 - содержащий @ НЕ содержащий домен верхнего уровня "mymail@"
         3 - содержащий несколько символов @ содержащий домен верхнего уровня "mymail@mail.ru"
         4 - содержащий несколько символов @ НЕ содержащий домен верхнего уровня "mymail@mail@ru"
         5 - минимальной длины "1@r"
         6 - пустой адрес ""
         7 - содержащий @ содержащий домен верхнего уровня "r1@mail.ru"
         сделаем массив checkString[0][0..6] - адреса для проверок
                        checkString[1][0..6] - текст с описанием ошибки
         */
        String[][] checkString = {{"mail.ru","mymail@","mymail@mail.ru","mymail@mail@ru","1@r","","r1@mail.ru"}, // адреса для проверки
                                  {"ошибка 1","ошибка 2","ошибка 3","ошибка 4","ошибка 5","ошибка 6","ошибка 7"}}; // что должны выдавать ошибки

        for(int i=0; i< checkString[0].length; i++){
            // введем e-mail из массива
            Assert.assertEquals(driver.findElement(By.id("email")).isDisplayed(),true);
// !!!! тут надо очистить INPUT от предыдцщих попыток ввода адреса
            driver.findElement(By.id("email")).sendKeys(checkString[0][i]);

            // нажмем на кнопку отправить сообщение
            Assert.assertEquals(driver.findElement(By.cssSelector(".btn-lg")).isDisplayed(),true);
            driver.findElement(By.cssSelector(".btn-lg")).click();
// !!! НЕ знаю как отловить это всплывающее сообщение об ошибке в e-mail

            //немного подождем пробежки полосы прокрутки <class = progress-bar>
            try { // подождем
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test (description="тест на проверку автоматического заполнения полей адреса из СПИСКА после ввода адреса в строку поиска")
    public void testCheckAutoFillingAddressFields() {

        // Проверим есть ли строка поиска адреса
        Assert.assertEquals(driver.findElement(By.id("address")).isDisplayed(),true);
        // введем в неё адрес
        driver.findElement(By.id("address")).sendKeys("г Москва, ул Молдавская, д 4, кв 34");
        try { // подождем
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("address")).sendKeys(Keys.ENTER);

        // Теперь проверим что полный адрес разобрался по нужным полям
        // поле индекс
        Assert.assertEquals(driver.findElement(By.id("address-postal_code")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-postal_code")).getAttribute("value"),"121467");

        // поле регион
        Assert.assertEquals(driver.findElement(By.id("address-region")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-region")).getAttribute("value"),"г Москва");

        // поле город
        Assert.assertEquals(driver.findElement(By.id("address-city")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-city")).getAttribute("value"),"г Москва");

        // поле улица
        Assert.assertEquals(driver.findElement(By.id("address-street")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-street")).getAttribute("value"),"ул Молдавская");

        // поле дом
        Assert.assertEquals(driver.findElement(By.id("address-house")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-house")).getAttribute("value"),"д 4");

        // поле квартира
        Assert.assertEquals(driver.findElement(By.id("address-flat")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-flat")).getAttribute("value"),"кв 34");

    }

    @Test (description="тест на ОТСУТСТВИЕ автоматического заполнения полей ПРОИЗВОЛЬНОГО адреса после ввода его адреса в строку поиска")
    public void testCheckNotAutoFillingAddressFields() {

        // Проверим есть ли строка поиска адреса
        Assert.assertEquals(driver.findElement(By.id("address")).isDisplayed(),true);
        // !!!! введем в неё ПРОИЗВОЛЬНЫЙ адрес
        driver.findElement(By.id("address")).sendKeys("246022 РБ Гомель, ул.Победы, 17а, 3");
        try { // подождем
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("address")).sendKeys(Keys.ENTER);

        // Теперь проверим что полный адрес разобрался по нужным полям
        // поле индекс
        Assert.assertEquals(driver.findElement(By.id("address-postal_code")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-postal_code")).getAttribute("value"),"");

        // поле регион
        Assert.assertEquals(driver.findElement(By.id("address-region")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-region")).getAttribute("value"),"");

        // поле город
        Assert.assertEquals(driver.findElement(By.id("address-city")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-city")).getAttribute("value"),"");

        // поле улица
        Assert.assertEquals(driver.findElement(By.id("address-street")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-street")).getAttribute("value"),"");

        // поле дом
        Assert.assertEquals(driver.findElement(By.id("address-house")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-house")).getAttribute("value"),"");

        // поле квартира
        Assert.assertEquals(driver.findElement(By.id("address-flat")).isDisplayed(),true);
        Assert.assertEquals(driver.findElement(By.id("address-flat")).getAttribute("value"),"");

    }

}


