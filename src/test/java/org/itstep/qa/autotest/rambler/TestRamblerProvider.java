package org.itstep.qa.autotest.rambler;

import org.testng.annotations.DataProvider;


public class TestRamblerProvider {

    @DataProvider(name = "dataProviderPassword")
    public Object[][] dataProviderPasswordMethod(){
        return new Object[][]{
                {"qWwew3wq_34","Сложный пароль"},
                {"qWwew3w34","Пароль средней сложности"}
        };
    }

    @DataProvider(name = "dataProviderIncorrectPassword")
    public Object[][] dataProviderIncorrectPasswordMethod(){
        return new Object[][]{
                {"qW34qa","Пароль должен содержать от 8 до 32 символов, включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру"},
                {"qWwew3w34lkjdweooqjjakжыж","Вы вводите русские буквы"},
                {"qW34qa$%*()~","Символ \"~\" не поддерживается. Можно использовать символы ! @ $ % ^ & * ( ) _ - +"}
        };
    }

    @DataProvider(name = "dataProviderEqualsPassword")
    public Object[][] dataProviderEqualsPasswordMethod(){
        return new Object[][]{
                {"qWwew3wq_34","qWwew3wq_34"},
                {"qWwew3w34","qWwew3w34"}
        };
    }

    @DataProvider (name = "dataProviderIncorrectEmail")
    public Object[][] dataProviderErrorEmailMethod(){
        return new Object[][]{ // и теперь спокойно передаём 2 параметра в тест
                {"ni","Логин должен быть от 3 до 32 символов"},
                {"nickijnuahabzauazskllijabqywbvxnainquqxnb","Логин должен быть от 3 до 32 символов"},
                {"nick","Почтовый ящик недоступен для регистрации. Попробуйте другой"},
                {"nick@n#i$c%k*()","Недопустимый логин"}
        };
    }

    @DataProvider (name = "dataProviderEmail")
    public Object[][] dataProviderEmailMethod(){
        return new Object[][]{
                {"mailtutby"},
                {"ne1wq3aw5sed"}
        };
    }

    // @Test(dataProvider = "dataProviderIncorrectPhone")
    // public void testCheckValidationPhone(String value, String error)

}
