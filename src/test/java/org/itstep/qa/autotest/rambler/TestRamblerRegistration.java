package org.itstep.qa.autotest.rambler;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 На форме регистрации rambler реализовать следующие тесты:
 1.	Необходимые тесты на валидацию пароля
 2.	Тест на проверку совпадения паролей
 3.	Необходимые тесты на валидацию имени почтового ящика
 */

public class TestRamblerRegistration {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openStartPage (){
        driver.get("https://id.rambler.ru/login-20/mail-registration?back=https%3A%2F%2Fwww.rambler.ru%2F&rname=head&param=iframe&iframeOrigin=https%3A%2F%2Fwww.rambler.ru");
    }

    @AfterClass
    public void stop() {
        driver.manage().deleteAllCookies();
        driver.close();
    }


/*
        // есть поле password - Пароль должен содержать от 8 до 32 символов,
        // включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру
        Assert.assertEquals(driver.findElement(By.id("newPassword")).isDisplayed(),true);
        driver.findElement(By.id("newPassword")).sendKeys("paSsword13");

        // есть поле return password
        Assert.assertEquals(driver.findElement(By.id("confirmPassword")).isDisplayed(),true);
        driver.findElement(By.id("confirmPassword")).sendKeys("paSsword13");


 */
    @Test(dataProviderClass = TestRamblerProvider.class, dataProvider = "dataProviderIncorrectPassword")
    public void testCheckValidationIncorrectPassword(String valueLogin, String valueMessage) {

        //немного подождем
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // проверим загрузилась ли страница <Регистрация>
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/h2")).getText(),"Регистрация Рамблер/почты");

        // проверим что есть поле password
        Assert.assertEquals(driver.findElement(By.id("newPassword")).isDisplayed(),true);
        driver.findElement(By.id("newPassword")).sendKeys(valueLogin);

        // переместим фокус в другое поле
        Assert.assertEquals(driver.findElement(By.id("login")).isDisplayed(),true);
        driver.findElement(By.id("login")).click();

        //немного подождем
        try { // подождем
            Thread.sleep(3000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

        // проверим наличе поля с ошибкой при вводе e-mail
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div")).isDisplayed(),true);
        // проверим сообщение об ошибке
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div")).getText(),valueMessage);
    }



    @Test(dataProviderClass = TestRamblerProvider.class, dataProvider = "dataProviderIncorrectEmail")
    public void testCheckValidationIncorrectEmail(String valueLogin, String valueMessage) {

        //немного подождем
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // проверим загрузилась ли страница <Регистрация>
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/h2")).getText(),"Регистрация Рамблер/почты");

        // проверим что есть поле login (email)
        Assert.assertEquals(driver.findElement(By.id("login")).isDisplayed(),true);
        driver.findElement(By.id("login")).sendKeys(valueLogin);

        // переместим фокус в другое поле
        Assert.assertEquals(driver.findElement(By.id("newPassword")).isDisplayed(),true);
        driver.findElement(By.id("newPassword")).click();

        //немного подождем
        try { // подождем
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // проверим наличе поля с ошибкой при вводе e-mail
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[1]/section/div/div/div[2]")).isDisplayed(),true);
        // проверим сообщение об ошибке
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[1]/section/div/div/div[2]")).getText(),valueMessage);
    }


    @Test(dataProviderClass = TestRamblerProvider.class, dataProvider = "dataProviderEmail")
    public void testCheckValidationEmail(String valueLogin) {

        //немного подождем
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // проверим загрузилась ли страница <Регистрация>
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/h2")).getText(),"Регистрация Рамблер/почты");

        // проверим что есть поле login (email)
        Assert.assertEquals(driver.findElement(By.id("login")).isDisplayed(),true);
        driver.findElement(By.id("login")).sendKeys(valueLogin);

        // переместим фокус в другое поле
        Assert.assertEquals(driver.findElement(By.id("newPassword")).isDisplayed(),true);
        driver.findElement(By.id("newPassword")).click();

        //немного подождем
        try { // подождем
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
