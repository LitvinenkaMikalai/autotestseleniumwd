package org.itstep.qa.autotest.devby;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
На странице регистрации сайте dev.by написать следующие тесты
   1. Попытка регистрации с некорректным email
   2. Попытка регистарции с пустыми паролем
   3. Проверка наличия на странице регистрации всех
      обязательных элементов
*/

public class TestsRegistrationDevBy {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openStartPage (){
        driver.get("https://id.dev.by/@/welcome");
    }

    @AfterClass
    public void stop() {
        driver.manage().deleteAllCookies();
        driver.close();
    }

    @Test (description="На странице регистрации dev.by тест - Попытка регистрации с некорректным email")
    public void testIncorrectEmail() {
        // проверим что открыли страницу регистрации
        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        Assert.assertEquals(element.getText(),new String("Регистрация"),"Страничка регистрации dev.by не открылась");

        // проверим что поле для ввода e-mail существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Электронная почта"),"Поля для ввода e-mail НЕТ");
        element.sendKeys("autotest.ru");

        // пароль - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Пароль"),"Поля для ввода пароля НЕТ");
        element.sendKeys("password13");

        // username - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Юзернейм"),"Поля для ввода username НЕТ");
        element.sendKeys("user13");

        // номер телефона - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Номер телефона"),"Поля для ввода телефона НЕТ");
        element.sendKeys("+375293999999");

        // я принимаю условия пользовательского соглашения - проверим что <checkbox> существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[1]/div"));
        Assert.assertEquals(element.isEnabled(),true);
        element.click();

        // Попробуем зарегистрироваться
        // Проверим что кнопка <Зарегистрироваться> существует  нажмем на неё
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button"));
        Assert.assertEquals(element.isDisplayed(),true);
        element.click();

        // проверим что появилось поле о неккоректноти e-mail
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        Assert.assertEquals(element.getText(),new String("Введите корректный адрес электронной почты."),"Подписи о неккоректноти e-mail НЕТ");
    }

    @Test (description="На странице регистрации dev.by тест - Попытка регистарции с пустыми паролем")
    public void testEmptyPassword() {
        // проверим что открыли страницу регистрации
        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        Assert.assertEquals(element.getText(),new String("Регистрация"),"Страничка регистрации dev.by не открылась");

        // проверим что поле для ввода e-mail существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Электронная почта"),"Поля для ввода e-mail НЕТ");
        element.sendKeys("autotest@mail.ru");

        // пароль - проверим что поле существует -  и оставим его пустым
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Пароль"),"Поля для ввода пароля НЕТ");

        // username - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Юзернейм"),"Поля для ввода username НЕТ");
        element.sendKeys("user13");

        // номер телефона - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Номер телефона"),"Поля для ввода телефона НЕТ");
        element.sendKeys("+375293999999");

        // я принимаю условия пользовательского соглашения - проверим что <checkbox> существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[1]/div"));
        Assert.assertEquals(element.isEnabled(),true);
        element.click();

        // Попробуем зарегистрироваться
        // Проверим что кнопка <Зарегистрироваться> существует  нажмем на неё
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button"));
        Assert.assertEquals(element.isDisplayed(),true);
        element.click();

        // проверим что появилось поле о неккоректноти пароля
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        Assert.assertEquals(element.getText(),new String("Введите пароль."),"Подписи о вводе пароля НЕТ");

    }

    @Test (description="На странице регистрации dev.by тест - Проверка наличия на странице регистрации всех обязательных элементов")
    public void testExistObligatoryElements() {
        // проверим что открыли страницу регистрации
        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        Assert.assertEquals(element.getText(),new String("Регистрация"),"Страничка регистрации dev.by не открылась");

        // проверим что поле для ввода e-mail существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Электронная почта"),"Поля для ввода e-mail НЕТ");

        // пароль - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Пароль"),"Поля для ввода пароля НЕТ");

        // username - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Юзернейм"),"Поля для ввода username НЕТ");

        // номер телефона - проверим что поле существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder"),new String("Номер телефона"),"Поля для ввода телефона НЕТ");

        // я принимаю условия пользовательского соглашения - проверим что <checkbox> существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[1]/div"));
        Assert.assertEquals(element.getAttribute("class"),new String("checkbox__box")," checkbox для отметки принятия пользовательского соглашения НЕТ");
        // или так
        Assert.assertEquals(element.isEnabled(),true);

        // я принимаю условия пользовательского соглашения - проверим что подпись существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[2]"));
        Assert.assertEquals(element.getAttribute("class"),new String("checkbox-text__text"),"Подписи кнопки принятия пользовательского соглашения НЕТ");

        // Просмотр пользовательского соглашения - проверим что ссылка существует
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[2]/a"));
        Assert.assertEquals(element.getText(),new String("пользовательского соглашения"),"Ссылки для просмотра пользовательского соглашения НЕТ");

        // Проверим что кнопка <Зарегистрироваться> существует но пока недоступна
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button"));
        Assert.assertEquals(element.isDisplayed(),true);
        Assert.assertEquals(element.getAttribute("class"),new String("button button_disabled")," кнопки <Зарегистрироваться> НЕТ");
    }
}
