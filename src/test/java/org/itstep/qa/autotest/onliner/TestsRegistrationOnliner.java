package org.itstep.qa.autotest.onliner;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.*;

/**
 repository - https://LitvinenkaMikalai@bitbucket.org/LitvinenkaMikalai/autotestseleniumwd.git
 На странице регистрации сайте onliner написать следующие тесты
 1. Попытка регистрации с некорректным email (проверять текст
 ошибки
 2. Попытка регистрации с несовпадающими паролями
 3. Попытка регистрации с пустыми полями
*/

public class TestsRegistrationOnliner {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openStartPage (){
        driver.get("https://profile.onliner.by/registration/");
    }

    @AfterClass
    public void stop() {
        driver.manage().deleteAllCookies();
        driver.close();
    }

    @Test(description="На странице регистрации onliner.by тест - Попытка регистрации с некорректным email")
    public void testRegistrationWithIncorrectEmail() {

        // проверим что открыли страницу регистрации по xpath элемента
        WebElement element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Регистрация").toLowerCase(),"Страничка регистрации onliner.by не открылась");

        // проверим что поле для ввода e-mail существует
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Ваш e-mail").toLowerCase(),"Поля для ввода е-мыла НЕТ");
        element.sendKeys("autotest.ru");

        // Должна появиться надпись о неправильном e-mail адресе
        // без ожидания этот элемент поймать мне и не удавалось!!! - на отладчике работает а на прямую нет
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Некорректный e-mail").toLowerCase(),"Предупреждение о некорректном знечении e-mail не выводиться");
    }

    @Test(description="Тест проверяет текст ошибки при попытке регистрации с несовпадающими паролями на onliner.by")
    public void testRegistrationDifferentPasswordsOnliner() {

        // проверим что открыли страницу регистрации по xpath элемента
        WebElement element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Регистрация").toLowerCase(),"Страничка регистрации onliner.by не открылась");

        // проверим что существует проверим что поле для ввода e-mail существует
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Ваш e-mail").toLowerCase(),"Поля для ввода е-мыла НЕТ");
        element.sendKeys("nickelpcvc@mail.ru");

        // проверим что существует элемент с вводом пароля
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Придумайте пароль").toLowerCase(),"Поля для ввода пароля НЕТ");
        element.sendKeys("aBcDiFg84");
        element.sendKeys(Keys.ENTER);

        // проверим что существует элемент с повтором пароля 2
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Повторите пароль").toLowerCase(),"Поля для повтора пароля НЕТ");
        element.sendKeys("cDaBg84iF");


        // После небольшого ожидания должна появиться надпись о несовпадениии паролей
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // проверим что существует элемент с предупреждением несовпадения паролей
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Пароли не совпадают").toLowerCase(),"Предупреждение о несовпадении паролей не выводиться");
    }


    @Test(description="На странице регистрации onliner.by тест - Попытка регистрации с пустыми полями")
    public void testRegistrationWithEmptyField() {
        // проверим что открыли страницу регистрации по xpath элемента
        WebElement element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Регистрация").toLowerCase(),"Страничка регистрации onliner.by не открылась");

        // проверим что поле для ввода e-mail существует
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Ваш e-mail").toLowerCase(),"Поля для ввода е-мыла НЕТ");


        // проверим что существует элемент с вводом пароля
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Придумайте пароль").toLowerCase(),"Поля для ввода пароля НЕТ");


        // проверим что существует элемент с повтором пароля 2
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input"));
        Assert.assertEquals(element.getAttribute("placeholder").toLowerCase(),new String("Повторите пароль").toLowerCase(),"Поля для повтора пароля НЕТ");

        // проверим что существует кнопка <Зарегистрироваться>
        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Зарегистрироваться").toLowerCase(),"Кнопки <Зарегистрироваться> НЕТ");
        // Теперь нажмем на кнопку <Зарегистрироваться>
        element.click();

        // После небольшого ожидания проверим появление надписей о пустых полях
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Укажите e-mail").toLowerCase(),"Предупреждение о вводе e-mail НЕТ");

        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Укажите пароль").toLowerCase(),"Предупреждение о вводе пароля НЕТ");

        element = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText().toLowerCase(),new String("Укажите пароль").toLowerCase(),"Предупреждение о повторе пароля НЕТ");
    }
}
